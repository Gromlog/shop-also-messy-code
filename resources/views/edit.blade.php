@extends('layouts.common')

@section('content')
<div class = "card">
    <div class = "card-body">
{!! Form::open(['url' => '/admin/edit/submit']) !!}
    <div class = "form-group">
    	{{ Form::label('imagepath', 'Picture Url')}}
    	{{ Form::text('imagepath', $product ->imagePath, ['class' => 'form-control', 'placeholder'])}}
    </div>
    <div class = "form-group">
    	{{ Form::label('title', 'Title')}}
    	{{ Form::text('title', $product -> title,['class' => 'form-control', 'placeholder'])}}
    </div>
    <div class = "form-group">
    	{{ Form::label('description', 'Description')}}
    	{{ Form::textarea('description', $product ->description, ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
    	{{ Form::label('price', 'Current price: '. $product ->price)}}
    	{{ Form::text("price",'' , ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
        {{ Form::label('amount', 'Stock|Amount')}}
        {{ Form::text('amount', $product ->amount, ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
        {{Form::label('sku', 'Sku')}}
        {{Form::text('sku', $product ->sku, ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
        {{Form::label('specialprice', 'Special offer price: '.$product ->specialprice.' leave it 0 if not needed')}}
        {{Form::text('specialprice', '', ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
        {{Form::label('status', 'Status')}}</br>
        {{Form::select('status', array('1' => 'Enabled', '0' => 'Disabled'))}}
        {{Form::hidden('id', $product->id, ['class' => 'form-control', 'type' => 'hidden'])}}
    </div>
    <div>
    	{{Form::submit('Submit',['class' => 'btn btn-primary'])}}
    </div>

{!! Form::close() !!}
</div>
</div>

@endsection