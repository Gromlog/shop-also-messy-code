@extends('layouts.common')
@section('content')

{!! Form::open(['url' => 'contact/submit']) !!}
<h1> Contact</h1>
    <div class = "form-group">
    	{{ Form::label('name', 'Name')}}
    	{{ Form::text('name', $user, ['class' => 'form-control', 'placeholder' => 'Enter name'])}}
    </div>
    <div class = "form-group">
    	{{ Form::label('email', 'Email')}}
    	{{ Form::text('email', $email, ['class' => 'form-control', 'placeholder' => 'Enter e-mail'])}}
    </div>
    <div class = "form-group">
    	{{ Form::label('message', 'Message')}}
    	{{ Form::textarea("message", "", ['class' => 'form-control', 'placeholder' => 'Enter your message'])}}
    </div>
    @include('inc.captcha')
    <div>
    	{{Form::submit('Submit',['class' => 'btn btn-primary'])}}
    </div>


{!! Form::close() !!}

@endsection