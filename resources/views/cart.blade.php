@extends('layouts.common')


@section('content')
<?php $price = 0;?>
<table class = "table text-center">
	<thead>
	<tr>
		<th scope = "col">Product</th>
		<th scope = "col">Title</th>
		<th scope = "col">Quantity</th>
		<th scope = "col">Price</th>
		<th scope = "col"></th>
	</tr>
</thead>
@if(count($products) > 0)

	@foreach($products as $product)
  		@foreach($productinfo as $productI)
  			@if($product->productid === $productI->id)
  			<tbody>
	      			<tr>
	      			<td>	
	      				<a href = "/detailed/{{$productI->id}}">
	      					<img style = "height:50px; width:70px;" src="{{$productI->imagePath}}" alt = "{{$productI -> title}}">
	      				</a>
	      			</td>
					<td>{{$productI -> title}}</td>
					<td>
						{!! Form::open(['url' => '/cart/quantityControl']) !!}
							<div class = "float-center">
								<div class = "form-group">
									{{Form::submit('', ['hidden' => 'hidden', 'name' => 'enter'])}}
									{{Form::submit('-', ['class' => 'btn btn-danger btn-number', 'name' => 'decrement'])}}
									{{Form::input('quantity', 'quantity', $product -> quantity,['size' => '2'])}}
									{{Form::submit('+', ['class' => 'btn btn-success btn-number', 'name' => 'increment'])}}
									{{Form::hidden('id', $product->id)}}
									{{Form::hidden('stock', $productI->amount)}}
								</div>
							</div>
						{!! Form::close() !!}
					</td>
					<td><?php if($productI ->specialprice > 0){
						echo $productI ->specialprice. '€';
						$price += $productI ->specialprice * $product ->quantity;
					}
                     	else{ 
                     		echo $productI->price. '€';
                 			$price += $productI ->price * $product ->quantity;
                     	}?>
                     	</td>
					<td>
						{!! Form::open(['url' => '/cart/delete']) !!}
    					<div class = "form-group">
    						{{Form::hidden('id', $product->id)}}
    						{{Form::submit('Delete',['class' => 'btn btn-link'])}}
   					 	</div>
   					 	{!! Form::close() !!}
					</td>
				</tr>
			@endif
		@endforeach
	@endforeach
@endif
<tr>
	<td></td><td></td><td></td><td>Total:{{$price}}€</td>
</tr>
<tbody>
</table>
<?php if($price > 0){
echo '<a href = "/cart/checkout" class = "float-right btn btn-secondary">Checkout</a>';
}?>

@endsection
@section('sidebar')

@endsection