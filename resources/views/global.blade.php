@extends('layouts.common')

@section('content')
@include('inc.admintab')
<div class = "card">
    <div class = "card-body">
{!! Form::open(['url' => '/admin/global/submit']) !!}
<h2 class = "text-center">Global Control</h2>
    <div class = "form-group">
        {{ Form::label('tax', 'Tax: 0-100%, % symbol not required')}}
        {{ Form::text('tax', $global->tax , ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
        {{ Form::label('taxstate', 'Apply tax?')}}
        {{ Form::checkbox('taxstate', '1', $global->taxstate)}}
    </div>
    <div class = "form-group">
        {{ Form::label('globaldiscount', 'Global discount 1-100%, % symbol not required')}}
        {{ Form::text('globaldiscount', $global->globaldiscount, ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
        {{ Form::label('globaldiscountremove', 'Would you like to exterminate global discount?')}}
        {{ Form::checkbox('globaldiscountremove', '1')}}
    </div>
    <div>
        {{Form::submit('Submit',['class' => 'btn btn-primary'])}}
    </div>

{!! Form::close() !!}
</div>
</div>

@endsection