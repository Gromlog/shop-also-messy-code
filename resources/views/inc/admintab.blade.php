<ul class="nav nav-pills">
  <li class="nav-item ">
    <a class="nav-link {{Request::is('admin')? 'active': ''}}" href="/admin">Product management</a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{Request::is('newproduct')? 'active': ''}}" href="/newproduct">Add product</a>
  </li>
    <li class="nav-item">
    <a class="nav-link {{Request::is('admin/global')? 'active': ''}}" href="/admin/global">Global control</a>
  </li>
</ul>