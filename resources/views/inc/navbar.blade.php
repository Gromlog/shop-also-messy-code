<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
  <div class="container">
    <a class="navbar-brand" href="{{ url('/') }}">
      {{ config('app.name', 'Laravel') }}
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <!-- Left Side Of Navbar -->
      <ul class="navbar-nav mr-auto">
        <li class="nav-item {{Request::is('/')? 'active': ''}}">
          <a class="nav-link" href="/">Home<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item {{Request::is('products')? 'active': ''}}">
          <a class="nav-link" href="/products">Products<span class="sr-only">(current)</span></a>
        </li> 
        <li class="nav-item {{Request::is('about')? 'active': ''}}">
          <a class="nav-link" href="/about">About <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item {{Request::is('contact')? 'active': ''}}">
          <a class="nav-link" href="/contact">Contact<span class="sr-only">(current)</span></a>
        </li>  
        <li class="nav-item {{Request::is('messages')? 'active': ''}}">
          <a class="nav-link" href="/messages">Other ppl messages<span class="sr-only">(current)</span></a>
        </li>
      </ul>

      <!-- Right Side Of Navbar -->
      <ul class="navbar-nav ml-auto">
        <ul class = "navbar-nav ml-auto">
          <li class="nav-item {{Request::is('cart')? 'active': ''}}">
            <a class="nav-link" href="/cart"><img src = "https://cdn0.iconfinder.com/data/icons/webshop-essentials/100/shopping-cart-512.png" style = "max-height:30px;">Cart<span class="sr-only">(current)</span></a>
          </li>
        </ul>
        <!-- Authentication Links -->
        @guest
          <li class="nav-item {{Request::is('login')? 'active': ''}}">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
          </li>
          @else
            <li class="nav-item dropdown">
              <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
                <img src="/storage/avatars/{{Auth::user()->avatar}}" class = "rounded-cirlce profile-image"/>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
                <a class = "dropdown-item" href = "/settings">Profile</a>
                <a class = "dropdown-item" href = "/admin">Admin</a
              </div>
            </li>
        @endguest
      </ul>
    </div>
  </div>
</nav>