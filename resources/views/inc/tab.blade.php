<ul class="nav nav-pills">
  <li class="nav-item ">
    <a class="nav-link {{Request::is('settings')? 'active': ''}}" href="/settings">Password</a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{Request::is('settings/changepic')? 'active': ''}}" href="/settings/changepic">Avatar</a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{Request::is('settings/billing')? 'active': ''}}" href="/settings/billing">Billing information</a>
  </li>
</ul>