@extends('layouts.common')

@section('content')
@include('inc.admintab')
<div class = "card">
	<div class = "card-body">
	<table class = "table text-center">
		<thead>
			<tr>
				<th scope = "col">Product</th>
				<th scope = "col">Title</th>
				<th scope = "col">Stock</th>
				<th scope = "col">Price</th>
				<th scope = "col">Sku</th>
				<th scope = "col"></th>
				<th scope = "col"></th>
			</tr>
		</thead>
		@if(count($products) > 0)
			<tbody>
				@foreach($products as $product)
	      			<tr>
	      			<td>	
	      				<a href = "/detailed/{{$product->id}}">
	      					<img style = "height:50px; width:70px;" src="{{$product->imagePath}}" alt = "{{$product -> title}}">
	      				</a>
	      			</td>
					<td>{{$product -> title}}</td>
					<td>{{$product-> amount}}</td>
					<td>
						<?php if($product ->specialprice > 0)echo $product ->specialprice. '€';
                     	else echo $product->price. '€';?>
					</td>
					<td>{{$product->sku}}</td>
					<td>
						{!! Form::open(['url' => '/admin/delete']) !!}
							<div class = "form-group">
								{{Form::hidden('id', $product->id)}}
								{{Form::submit('Delete',['class' => 'btn btn-link'])}}
							</div>
						{!! Form::close() !!}
					</td>
					<td>
		 				<div class = "form-group">
		 					<a href="/admin/edit/{{$product ->id}}" class = "btn btn-link">Edit</a>
						</div>
				 		
					</td>
				</tr>
			@endforeach
		@endif
	</tbody>
</table>
</div>
</div>
@endsection