@extends('layouts.common')


@section('content')
@include('inc.tab')
  	<div class="row card"></br>
      	<h2 class="col-md-6 mb-2">Billing address</h2>
	      {!! Form::open(['url' => '/settings/billing/submit']) !!}
            <div class = "form-group col-md-6">
				{{ Form::label('firstname', 'First name')}}
    		{{ Form::text('firstname', $firstname, ['class' => 'form-control'])}}
    		</div>
        <div class = "form-group col-md-6">
				{{ Form::label('lastname', 'Last name')}}
    		{{ Form::text('lastname', $lastname, ['class' => 'form-control'])}}
        </div>
			<div class = "form-group col-md-6">
				{{ Form::label('country', 'Country')}}
    		{{ Form::text('country', $country, ['class' => 'form-control'])}}
			</div>
			<div class = "form-group col-md-6">
				{{ Form::label('city', 'City')}}
    		{{ Form::text('city', $city, ['class' => 'form-control'])}}
			</div>
			<div class = "form-group col-md-6">
				{{ Form::label('address', 'Address')}}
    			{{ Form::text('address', $address, ['class' => 'form-control'])}}
			</div>
			<div class = "form-group col-md-6">
				{{ Form::label('zip', 'Zip')}}
    		{{ Form::text('zip', $zip, ['class' => 'form-control'])}}
			</div>
      <div class = "form-group col-md-6">
        @include('inc.captcha')
      </div>
		    <div class = "col-md-6">
    			{{Form::submit('Save information',['class' => 'btn btn-primary'])}}
    		</div>
  	</div>
{!! Form::close() !!}



@endsection