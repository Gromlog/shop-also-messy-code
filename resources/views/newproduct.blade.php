@extends('layouts.common')

@section('content')
@include('inc.admintab')
<div class = "card">
    <div class = "card-body">
{!! Form::open(['url' => 'newproduct/submit']) !!}
<h1> New product</h1>

    <div class = "form-group">
    	{{ Form::label('imagepath', 'Picture Url')}}
    	{{ Form::text('imagepath', "", ['class' => 'form-control', 'placeholder'])}}
    </div>
    <div class = "form-group">
    	{{ Form::label('title', 'Title')}}
    	{{ Form::text('title', "",['class' => 'form-control', 'placeholder'])}}
    </div>
    <div class = "form-group">
    	{{ Form::label('description', 'Description')}}
    	{{ Form::textarea('description', "", ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
    	{{ Form::label('price', 'Price')}}
    	{{ Form::text("price", "", ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
        {{ Form::label('amount', 'Stock|Amount')}}
        {{ Form::text('amount', "", ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
        {{Form::label('sku', 'Sku')}}
        {{Form::text('sku', '', ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
        {{Form::label('specialprice', 'Special price, leave it 0 if not needed')}}
        {{Form::text('specialprice', '0', ['class' => 'form-control'])}}
    </div>
    <div class = "form-group">
        {{Form::label('status', 'Status')}}</br>
        {{Form::select('status', array('1' => 'Enabled', '0' => 'Disabled'))}}
    </div>
    <div class = "form-group">
        {{Form::label('category', 'Category')}}</br>
        {{Form::select('category', array('video' => 'Video', 'computers' => 'Computers', 'phones' => 'Phones', 'headphones' => 'Headphones'))}}
    </div>
    <div>
    	{{Form::submit('Submit',['class' => 'btn btn-primary'])}}
    </div>

{!! Form::close() !!}
</div>
</div>

@endsection