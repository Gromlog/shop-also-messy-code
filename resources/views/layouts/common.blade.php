<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<title>Laravel</title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="{{ asset('\js\app.js') }}" defer></script>
	<link rel="stylesheet" type="text/css" href="\css\app.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


</head>
<body>
	@include('inc.navbar')
	<div id = "app" class = "container">

		<div class = "row">
			<div class = "col-md-10 col-lg-10">
				@if(Request::is('home') || Request::is('/'))
					@include('inc.showcase')
				@endif
				@include('inc.errors')
				@yield('content')
			</div>
			<div class = "col-md-2 col-lg-2">
				@include('inc.sidebar')
			</div>
		</div>
	</div>
	<footer class = "text-center">
		<p>Copyright 2019 &copy; Learn</p>
	</footer>
</body>
</html>