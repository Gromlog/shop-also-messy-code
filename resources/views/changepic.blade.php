@extends('layouts.common')
@section('content')    
        @include('inc.tab')
    <div class="card">
        <div class = "card-body">
            <div class="row justify-content-center">
                <div class="profile-header-container">
                    <div class="profile-header-img">
                        <img class="rounded-circle" style = "width:300px; height:300px;" src="/storage/avatars/{{ $user->avatar }}" />
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <form action="/pic_update" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <input type="file" class="form-control-file" name="avatar" id="avatarFile" aria-describedby="fileHelp">
                        <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.Gifs are uploadable too</small>
                    </div>
                    @include('inc.captcha')
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection