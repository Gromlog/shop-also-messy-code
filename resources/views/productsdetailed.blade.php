@extends('layouts.common')

@section('content')
<div class = "row">
   <div class = "col-md-5">
      <div class = "thumbnail">
         <img class = "detailedSize img-responsive" src = "{{$product -> imagePath}}" alt = "Generic placeholder thumbnail">
      </div>
   <div class = "caption">
      <h3>{{$product -> title}}</h3>
                  <?php if($product ->specialprice > 0)echo '<p>Price:<del>'.$product->price.' </del>'. $product ->specialprice.'€</p>';
                     else echo '<p>Price: '.$product->price.'€</p>';?>
            <p>Stock: {{$product->amount}}</p>
            <p>Sku: {{$product ->sku}}</p>
               {!! Form::open(['url' => '/cart/submit']) !!}
                   <div>
                     Quantity:{{Form::input('quantity', 'quantity','1',['class' => 'quantity'])}}</br>
                     {{Form::hidden('id', $product -> id, ['class' => 'form-control', 'type' => 'hidden'])}}
                     {{Form::hidden('stock', $product -> amount, ['class' => 'form-control', 'type' => 'hidden'])}}
                     {{Form::submit('Add to cart',['class' => 'btn btn-primary'])}}
                   </div>
                   {!! Form::close()!!}
      </div>
   </div> 
   <div class = "col-md-7">
      <div class = "card">
         <p class = "card-body card-text">{{$product -> description}}</p>
      </div>
   </div>
</div>
<h2 class = "text-center">Reviews</h2>
@if(count($messages) > 0)
   @foreach($messages as $message)
   <div class = "row" style = "padding:10px;">
      <div class = "col-md-3 card">
         <p>{{$message->name}}</p>

         <p>
            Rating: @for($i = 0;$i < $message->rating;$i++)
               &#11088
            @endfor
         </p>
         <p>{{$message->created_at}}</p>
      </div>
      <div class = "col-md-9 card">
         {{$message->message}}
      </div>
   </div>
   @endforeach
   @else
      <p class = "text-center">No reviews, be first to write one</p>
@endif
<div class = "row" style = "padding: 20px;">
   <div class = "col-md-3"></div>
   <div class = "col-md-9">
   {!!Form::open(['url' => '/detailed/review/submit'])!!}
         {{Form::label('name', 'Name')}}
   <div class = "form-group">
      {{Form::text('name','',['class' => 'form-control'])}}
   </div>
   {{Form::label('message','Comment')}}
   <div class = "form-group">
      {{Form::textarea('message', '',['class' => 'form-control'])}}
   </div>
   {{Form::label('rating', 'Rating')}}
   <div class = "form-group">
      {{Form::select('rating', array('5' => '5', '4' => '4', '3' => '3', '2' => '2', '1' => '1'),'' ,['width' =>'6'])}}
   </div>
   {{Form::hidden('id', $product -> id, ['class' => 'form-control', 'type' => 'hidden'])}}
   @include('inc.captcha')
   {{Form::submit('Send review',['class' => 'btn btn-primary'])}}
   {!! Form::close()!!}
   </div>
</div>
@endsection

@section('sidebar')


@endsection