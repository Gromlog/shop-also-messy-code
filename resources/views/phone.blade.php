@extends('layouts.common')

@section('content')
<div class = "row" style = "padding:30px;">
   @if(count($products) > 0)
      @foreach($products as $product)
         <div class = " col-sm-6 col-md-3" style = "height:400px;">
            <a href="/detailed/{{$product -> id}}">
            <div class = "thumbnail">
               <img class = "size img-responsive" src = "{{$product -> imagePath}}" alt = "Generic placeholder thumbnail">
         </div>
         <div class = "caption">
            <h3>{{$product -> title}}</h3>
         </a>
               <span style = "bottom:0; position:absolute;">
                  <?php if($product ->specialprice > 0)echo '<p>Price:<del>'.$product->price.' </del>'. $product ->specialprice.'€</p>';
                     else echo '<p>Price: '.$product->price.'€</p>';?>
                  <p>Stock: {{$product -> amount}}</p>
                  {!! Form::open(['url' => '/cart/submit']) !!}
                      <div>
                        Quantity:
                        {{Form::input('quantity', 'quantity','1',['class' => 'quantity', 'size' => '2'])}}</br>
                        {{Form::hidden('id', $product -> id, ['class' => 'form-control', 'type' => 'hidden'])}}
                        {{Form::hidden('stock', $product -> amount, ['class' => 'form-control', 'type' => 'hidden'])}}
                        {{Form::submit('Add to cart',['class' => 'btn btn-primary'])}}
                   </div>
               {!! Form::close() !!}
            </span>
         </div>
      </div>   
      @endforeach
   @endif  
</div>
@endsection


@section('sidebar')

@endsection