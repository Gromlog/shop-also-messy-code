<?php

Route::get('/home', 'pagesController@getHome')->name('home')->middleware('verified');
Route::get('/', 'pagesController@getHome')->name('home');
Route::get('/about', 'pagesController@getAbout');
Route::get('/contact', 'pagesController@getContact');

Route::get('/products', 'Products@retrieve'); // show products
Route::get('/products/headphones', 'Products@getHeadphones'); 
Route::get('/products/phones', 'Products@getPhones');
Route::get('/products/computers', 'Products@getComputers');
Route::get('/products/video', 'Products@getVideo');
Route::get('/detailed/{id}', 'Products@getDetailed');
Route::post('/detailed/review/submit', 'Products@SetReview');

Route::get('/cart', 'Products@getCart'); // cart stuff
Route::post('/cart/submit', 'Products@Add');
Route::post('/cart/delete', 'Products@Remove');
Route::post('/cart/quantityControl', 'Products@quantityControl');
Route::get('/cart/checkout', 'checkoutcontroller@Checkout');

Route::post('/paypal', 'paypalController@payWithpaypal')->name('paywithpaypal'); // paypal stuff
Route::get('status', 'paypalController@getPaymentStatus')->name('status'); 

Route::get('/admin', 'authorisedOnly@getAdmin'); 
Route::get('/newproduct', 'authorisedOnly@getProductsn'); // new product form
Route::post('/newproduct/submit', 'authorisedOnly@submitProduct');

Route::post('/admin/delete', 'authorisedOnly@deleteProduct');
Route::get('/admin/edit/{id}', 'authorisedOnly@editView');
Route::post('/admin/edit/submit', 'authorisedOnly@editProduct');

Route::get('/admin/global', 'authorisedOnly@globalControlView');
Route::post('/admin/global/submit', 'authorisedOnly@globalControlSet');

Route::get('/settings','authorisedOnly@getSettings'); // pass change
Route::post('/settings','authorisedOnly@changePassword')->name('changePassword');

Route::get('/settings/changepic','authorisedOnly@getPicture'); // avatar stuff
Route::post('/pic_update', 'authorisedOnly@update_avatar'); 

Route::get('/settings/billing', 'authorisedOnly@getBilling'); // billing
Route::post('/settings/billing/submit', 'authorisedOnly@saveBilling'); 

Route::get('/messages', 'MessagesController@retrieve'); // contacts
Route::post('/contact/submit', 'MessagesController@submit');

Auth::routes(['verify' => true]);