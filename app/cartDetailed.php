<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class cartDetailed extends Model
{
	public static function Get(){
		if(Auth::guest())$person = session()->getid();
		else $person = Auth::id();

	 	return cartDetailed::all()->where('cartidentifier', $person);
	}
	public static function Add($request, $person){
		if($request['quantity'] > 0 && $request['quantity'] <= $request['stock']){
			if(cartDetailed::all()->where('productid', $request['id'])->where('cartidentifier', $person)->first()){
				$cart = cartDetailed::where('productid', $request['id'])->where('cartidentifier', $person)->first();
			}
			else{
				$cart = new cartDetailed;	
				$cart->productid = $request['id'];
				$cart->cartidentifier = $person;
			}
			$cart->quantity = $request['quantity'];
			$cart->save();
			return "success";
		}

	}
    public static function Remove($request){
    	$remove = cartDetailed::all()->where('id', $request['id'])->first();
		$remove->delete();
    }
    public static function QuantityControl($request){
    	$product = cartDetailed::where('id', $request['id'])->first();
		if($request['increment']) $product ->quantity += 1;
		elseif($request['decrement']) $product ->quantity -=1;
		elseif(is_int($request['quantity']) <= $request['stock']) $product ->quantity = $request['quantity'];

		if($product->quantity > 0 && $product->quantity <= $request['stock']){
			$product ->save();
			return "success";	
		} 
    }
}
