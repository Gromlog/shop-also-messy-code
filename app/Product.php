<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\globalControl;
use App\Review;
use App\cartDetailed;
class Product extends Model
{
   	public static function Add($request){
   		$product = new Product;
   		$product ->imagepath = $request->input('imagepath');
   		$product ->title = $request->input('title');
   		$product ->description = $request->input('description');
   		$product ->price = globalControl::IndividualTax($request['price']);
   		$product ->category = $request->input('category');
   		$product ->amount = $request->input('amount');
         $product ->sku = $request->input('sku');
         $product ->status = $request->input('status');
         $product ->specialprice = globalControl::IndividualTax($request['specialprice']);
   		$product ->save();
   	}
      public static function UpdateP($request){
         $product = Product::all()->where('id', $request['id'])->first();
         if($request['price'] != null) $product ->price = globalControl::IndividualTax($request['price']);
         if($request['specialprice'] != null) $product ->specialprice = globalControl::IndividualTax($request['specialprice']);
         $product ->imagepath = $request->input('imagepath');
         $product ->title = $request->input('title');
         $product ->description = $request->input('description');
         $product ->amount = $request->input('amount');
         $product ->sku = $request->input('sku');
         $product ->status = $request->input('status');
         $product ->save();
      }
      public static function Remove($request){
         $remove = Product::all()->where('id', $request['id'])->first();
         $remove->delete();
      }
   	public static function Detailed($id){
   		return Product::where('id', $id)->first();
   	}
   	public static function ReturnCategory($category){
   		return Product::all()->where('category', $category)->where('status', '1');
   	}
}