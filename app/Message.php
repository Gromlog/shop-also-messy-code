<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    Public static function Set(){
    	     $data = [
            'user' => '',
            'email' => '',
        ];
    	if(Auth::user()){
    		$data = [
            'user' => Auth::user()->name,
            'email' => Auth::user()->email,
        ];
    	}
    	return $data;
    }
    public static function send($request){
    	$message = new Message;
    	$message ->name = $request->input('name');
    	$message ->email = $request->input('email');
    	$message ->message = $request->input('message');
    	$message ->save();
    }
}
