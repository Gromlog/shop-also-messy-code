<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class billingAddress extends Model
{
    public static function AddOrUpdate($request){
        $info = new billingAddress;
        if(billingAddress::all()->where('identifier', Auth::id())->first()){
            $info = billingAddress::all()->where('identifier', Auth::id())->first();
        }
        $info->firstname = $request->input('firstname');
        $info->lastname = $request->input('lastname');
        $info->country = $request->input('country');
        $info->city = $request->input('city');
        $info->address = $request->input('address');
        $info->zip = $request->input('zip');
        $info->identifier = Auth::id();
        $info->save();
    }
    public static function Get(){
    	if(billingAddress::all()->where('identifier', Auth::id())->first()){
            $info = billingAddress::all()->where('identifier', Auth::id())->first();
            $data = [
                'firstname' => $info->firstname,
                'lastname' => $info->lastname,
                'country' => $info->country,
                'city' => $info->city,
                'address' => $info->address,
                'zip' => $info->zip,
            ];
        }
        else{
            $data = [
                'firstname' => '',
                'lastname' => '',
                'country' => '',
                'city' => '',
                'address' => '',
                'zip' => '',
            ];
		}
    return $data;
	}
}
