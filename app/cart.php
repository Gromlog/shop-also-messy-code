<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class cart extends Model
{
    public static function CheckOrCreate(){
    	if(!Auth::guest()){	 
			$person = Auth::id();
			if(!cart::all()->where('userId', Auth::id())->first()){ // find existing cart or create a new one according to user
				$newCart = new cart;
				$newCart->userId = $person;
				$newCart->save();	
			}
		}
		else{
			$person = session()->getid();
			if(!cart::all()->where('userId', $person)->first()){
				$newCart = new cart;
				$newCart->userId = $person;
				$newCart->save();
			}
		}
		return $person; 
    }
}
