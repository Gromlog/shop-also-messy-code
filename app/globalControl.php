<?php

namespace App;
use App\Product;

use Illuminate\Database\Eloquent\Model;

class globalControl extends Model
{
    public static function get(){
    	$info = globalControl::all()->first();
    	return $info;
    }

    public static function Set($request){
    	$info = globalControl::all()->first();
    	$Products = Product::all();
    	if($request['globaldiscountremove'] === '1'){
    		$request['globaldiscount'] = 0;
    		foreach($Products as $Product){
    			$Product->specialprice = 0;
    			$Product->save();
    		}
    	}
    	if($info ->taxstate === 1 && $info->tax != 0){ // rollback price to it's original if tax state is about to change
    		foreach($Products as $Product){
    			$Product->price = round($Product->price / (1 + ($info ->tax / 100)), 2); 
    			if($Product->specialprice > 0 && $request['globaldiscount'] === '0') $Product->specialprice = round($Product->specialprice / (1 + ($info ->tax / 100)), 2); 
    			$Product->save();
    		}
    	}
    	if($request['taxstate'] === '1' && $request['tax'] > 0){ // set tax 
    		foreach($Products as $Product){
    			$Product->price = round($Product ->price * (1 + ($request['tax'] / 100)), 2);
    			if($Product->specialprice > 0 && $request['globaldiscount'] === '0') $Product->specialprice = round($Product ->specialprice * (1 + ($request['tax'] / 100)), 2);
    			$Product->save();
    		}
    	}
    	if($request['globaldiscount'] > 0){
    		foreach($Products as $Product){
    			$Product->specialprice = round($Product->price / (1+ ($request['globaldiscount'] / 100)),2);
    			$Product->save();
    		}
    	}
    	if($request['taxstate'] === '1')$info->taxstate = $request['taxstate'];
    	else $info ->taxstate = 0;
    	$info->tax = $request['tax'];
    	$info->globaldiscount = $request['globaldiscount'];
    	$info->save(); 
    }
    public static function IndividualTax($request){
    	$info = globalControl::all()->first();
    	if($info ->tax > 0 && $info ->taxstate > 0) $price = round($request * (1 + ($info ->tax / 100)), 2);
    	else $price = $request;
    	return $price;
    }
}
