<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public static function Get($request){
    	return Review::All()->where('productid', $request); 
    }
    public static function Set($request){
    	$Review = new Review;
    	$Review->name = $request['name'];
    	$Review->message = $request['message'];
    	$Review->productid = $request['id'];
    	$Review->rating = $request['rating'];
    	$Review->save();
    }
}
