<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Rules\Captcha;
use Illuminate\Support\Facades\Hash;
use App\Message;

class pagesController extends Controller
{
    public function getHome(){
    	return view('home');
    }
    public function getAbout(){
    	return view('about');
    }
    public function getContact(){
    	return view('contact')->with(Message::Set());
    }
}
