<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Rules\Captcha;

class MessagesController extends Controller
{
    public function submit(Request $request){
    	$this -> validate($request, [
    		'name' => 'required',
    		'email' => 'required',
            'message' => 'required',
            'g-recaptcha-response' => new Captcha(),
    	]);
        Message::Send($request);
    	return redirect('/')->with('success','Message Sent');
    }

    public function retrieve(){
    	return view('messages')->with('messages', Message::all());

    }
}
