<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Rules\Captcha;
use App\cart;
use App\cartDetailed;
use App\User;
use session;
Use App\Review;
use Illuminate\Support\Facades\Auth;

class Products extends Controller
{
	public function retrieve(){ // all products
		return view('products')->with('products', Product::all()->where('status', '1'));
	}
	public function getDetailed($id = null){ // detailed information
		return view('productsdetailed')->with('product', Product::Detailed($id))->with('messages', Review::Get($id));
	}
	public function SetReview(Request $request){
		$this -> validate($request, [
    		'name' => 'required',
    		'message' => 'required',
            'g-recaptcha-response' => new Captcha(),
    	]);
		Review::Set($request);
		return redirect()->back();
	}
	
	public function getPhones(){ // categories
		return view('phone')->with('products', Product::ReturnCategory('phones'));
	}
	public function getVideo(){
		return view('video')->with('products', Product::ReturnCategory('video'));
	}
	public function getComputers(){ 
		return view('computers')->with('products', Product::ReturnCategory('computers'));
	}
	public function getHeadphones(){
		return view('headphones')->with('products', Product::ReturnCategory('headphones'));
	} // categories
	
	public function getCart(){ // cart view
		return view('cart')->with('products', cartDetailed::Get())->with('productinfo', Product::all()->where('status', '1'));
	}

	public function quantityControl(Request $request){ // cart quantity control
		$status = cartDetailed::QuantityControl($request);
		if($status === "success") return redirect('/cart');
		else return redirect('/cart')->with('failed', "Quantity must be greater than 0 and can't exceed our stock limit");
	}

	public function Add(Request $request){ // add to cart
		$person = cart::CheckOrCreate();
		$status = cartDetailed::Add($request, $person);
		if($status === "success") return redirect()->back()->with('success', 'Saved to cart');
		else return redirect()->back()->with('failed', "Quantity must be greater than 0 and can't exceed our stock limit");
	}

	public function Remove(Request $request){
		cartDetailed::Remove($request);
		return redirect('/cart');
	}
}
