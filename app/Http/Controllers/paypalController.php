<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Agreement;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use Illuminate\Support\Facades\URL;
use PayPal\Api\Payer;
use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\PayerInfo;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use Session;
use Redirect;
use PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\Input;

class paypalController extends Controller
{
    public function __construct()
    {
/** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
	}
	public function payWithpaypal(Request $request)
    {
		$payer = new Payer();
		        $payer->setPaymentMethod('paypal');
		$item_1 = new Item();
		$item_1->setName('Products')
			->setCurrency('EUR')
			->setQuantity(1)
			->setPrice($request['price']);
		$item_list = new ItemList();
		$item_list->setItems(array($item_1));

		$amount = new Amount();
		        $amount->setCurrency('EUR')
		        ->setTotal($request['price']);
		$transaction = new Transaction();
		$transaction->setAmount($amount)
				->setItemList($item_list)
		        ->setDescription('Thank you for buying our product(s)');
		        
		$redirect_urls = new RedirectUrls();
		        $redirect_urls->setReturnUrl(URL::route('status'))->setCancelUrl(URL::route('status'));
		$payment = new Payment();
		        $payment->setIntent('Sale')
		            ->setPayer($payer)
		            ->setRedirectUrls($redirect_urls)
		            ->setTransactions(array($transaction));
		        /** dd($payment->create($this->_api_context));exit; **/
		        try {
		$payment->create($this->_api_context);
		} catch (\PayPal\Exception\PPConnectionException $ex) {
		if (\Config::get('app.debug')) {
		\Session::put('error', 'Connection timeout');
		                return Redirect::route('paywithpaypal')->with('failed', 'Connection timeout');
		} else {
		\Session::put('error', 'Some error occur, sorry for inconvenient');
		                return Redirect::route('paywithpaypal')->with('failed', 'Some error occured');
		}
		}
		foreach ($payment->getLinks() as $link) {
			if ($link->getRel() == 'approval_url') {
				$redirect_url = $link->getHref();
	            break;
			}
		}
		/** add payment ID to session **/
		        Session::put('paypal_payment_id', $payment->getid());
		if (isset($redirect_url)) {
		/** redirect to paypal **/
		            return Redirect::away($redirect_url);
		}
		\Session::put('error', 'Unknown error occurred');
		        return Redirect::route('paywithpaypal')->with('failed', 'unknown error occured');
		}

	public function getPaymentStatus(){
		$payment_id = Session::get('paypal_payment_id');
		Session::forget('paypal_payment_id');
		if(empty(Input::get('PayerID')) || empty(Input::get('token'))){
			return redirect::to('/')->with('failed', 'Payment failed');
		}
		$payment = Payment::get($payment_id, $this ->_api_context);
		$execution = new PaymentExecution();
		$execution->setPayerId(Input::get('PayerID'));
		$result = $payment->execute($execution, $this->_api_context);

		if($result->getState() === 'approved'){
			return Redirect::to('/')->with('success', 'Transaction completed');
			// archive items remove current cart :) 
		}
		else{
			return Redirect::to('/')->with('failed', 'payment failed');
		}

	}
}
