<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\billingAddress;
use Auth;
use App\cartDetailed;
use App\cart;
use App\Product;

class checkoutcontroller extends Controller
{
    public function Checkout(){
        $data = [
            'firstname' => '',
            'lastname' => '',
            'country' => '',
            'city' => '',
            'address' => '',
            'zip' => '',
            'price' => '0',
        ];
        if(Auth::check()){
            $info = billingAddress::all()->where('identifier', Auth::id())->first();
            $data = [
                'firstname' => $info->firstname,
                'lastname' => $info->lastname,
                'country' => $info->country,
                'city' => $info->city,
                'address' => $info->address,
                'zip' => $info->zip,
            ];
        }
        $find = Product::all()->where('status', '1');
        $items = cartDetailed::get();
        $price = 0;
        foreach($find as $findprice){
            foreach($items as $item){
        		if($findprice->id === $item->productid){
                    if($findprice->specialprice > 0)$price += ($findprice->specialprice * $item->quantity);
                    else$price += ($findprice ->price * $item->quantity);
        		}
        	}
        }
        $data['price'] = $price;
        return view('checkout')->with($data);
    }
}
