<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Rules\Captcha;
use Illuminate\Support\Facades\Hash;
use App\billingAddress;
use App\Avatar;
use App\Product;
use App\ChangePwd;
use App\globalControl;

class authorisedOnly extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getProductsn(){
        return view('newproduct');
    }
    public function submitProduct(Request $request){ 
            $this -> validate($request, [
            'imagepath' => 'required',
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'amount' => 'required',
            'category' => 'required',
            'sku' => 'required',
            'status' => 'required',
        ]);
        Product::Add($request);
        return redirect('/newproduct')->with('success','Product registered');
    }
    public function deleteProduct(Request $request){
        Product::Remove($request);
        return redirect('/admin')->with('success', 'Product removed');
    }
    public function editView($id = null){
        return view('edit')->with('product', Product::Detailed($id));
    }
    public function editProduct(Request $request){
        $this -> validate($request, [
        'imagepath' => 'required',
        'title' => 'required',
        'description' => 'required',
        'amount' => 'required',
        'sku' => 'required',
        'status' => 'required',
        ]);
        Product::UpdateP($request);
        return view('admin')->with('success', 'Product successfully updated')->with('products', Product::All());
    }
    public function globalControlView(){
        return view('global')->with('global', globalControl::get());
    }
    public function globalControlSet(Request $request){
        globalControl::Set($request);
        return redirect()->back();
    }

    public function getPicture(){
        return view('changepic')->with('user', Auth::user());
    }
    public function getSettings(){
        return view('auth.settings');
    }
    public function getAdmin(){
        return view('admin')->with('products', Product::All());
    }
    public function getBilling(Request $request){ 
        return view('billing')->with(billingAddress::Get($request));
    }
    public function saveBilling(Request $request){
        $this->validate($request, [
        'firstname' => 'required',
        'lastname' => 'required',
        'country' => 'required',
        'city' => 'required',
        'address' => 'required',
        'zip' => 'required',
        'g-recaptcha-response' => new Captcha(),
        ]);
        billingAddress::AddOrUpdate($request);
        return redirect('/settings/billing')->with('success', 'Billing information saved');
    }
    public function update_avatar(Request $request){
        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'g-recaptcha-response' => new Captcha(),

        ]);
        Avatar::Upload($request);
        return back()->with('success','You have successfully uploaded the image.');
    }


    public function changePassword(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are the same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:8|confirmed',
            'g-recaptcha-response' => new Captcha(),
        ]);
        ChangePwd::ChangePass($request);
        return redirect()->back()->with("success","Password changed successfully !");
        }
}
