<?php

use Illuminate\Database\Seeder;

class globalControlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('global_controls')->insert([
            'tax' => '0',
            'taxstate' => '0',
            'globaldiscount' => '0',
        ]);
    }
}
